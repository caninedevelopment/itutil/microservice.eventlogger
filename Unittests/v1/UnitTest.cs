﻿namespace Unittests
{
    using Monosoft.Service.EventLogger.V1;
    using NUnit.Framework;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// SaveLoadSettingsDBPath
        /// </summary>
        [Test]
        public void SaveLoadSettingsDBPath() // can it save and load the dbPath
        {
            string testDbPath = string.Empty;
            Logic.SaveSettings(new Settings() { dbPath = testDbPath });
            var loaded = Logic.LoadSettings();
            Assert.AreEqual(testDbPath, loaded.dbPath, "Loaded settings should be the same as saved settings");
        }

        /// <summary>
        /// SaveLoadSettingsDBBaseName
        /// </summary>
        [Test]
        public void SaveLoadSettingsDBBaseName() // can it save and load the dbBaseName
        {
            string testDbBaseName = string.Empty;
            Logic.SaveSettings(new Settings() { dbBaseName = testDbBaseName });
            var loaded = Logic.LoadSettings();
            Assert.AreEqual(testDbBaseName, loaded.dbBaseName, "Loaded settings should be the same as saved settings");
        }
    }
}
