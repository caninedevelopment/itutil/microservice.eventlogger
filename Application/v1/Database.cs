﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace Monosoft.Service.EventLogger.V1
{
    /// <summary>
    /// Database
    /// </summary>
    public class Database
    {
        private readonly string dbName;
        private readonly string dbPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="Database"/> class.
        /// Setup the database
        /// </summary>
        /// <param name="name">string name</param>
        /// <param name="path">string path</param>
        public Database(string name, string path) // string dbName = "RabbitMQLogDB";
        {
            this.dbName = name;
            this.dbPath = path;
            if (!File.Exists(string.Format(ITUtil.Common.Constants.Culture.DefaultCulture, @"{0}{1}.db", this.dbPath, this.dbName)))
            {
                SQLiteConnection.CreateFile(string.Format(ITUtil.Common.Constants.Culture.DefaultCulture, @"{0}{1}.db", this.dbPath, this.dbName)); // Create a database named MyDatabase
                this.CreateTables();
            }
        }

        /// <summary>
        /// Writes a command to the dbSource and returns the id
        /// </summary>
        /// <param name="command">string command</param>
        /// <returns>long</returns>
        public long WriteToDBReturnID(string command)
        {
            long lastRowId = 0;
            SQLiteConnection dBConnect = new SQLiteConnection(string.Format(ITUtil.Common.Constants.Culture.DefaultCulture, "Data source = {0}{1}; Version = 3; ", this.dbPath, this.dbName));
            string sql = @"SELECT last_insert_rowid()";
            dBConnect.Open();
            SQLiteCommand Command = new SQLiteCommand(command, dBConnect);
            Command.ExecuteNonQuery();

            Command = new SQLiteCommand(sql, dBConnect); // get id of the inserted row
            lastRowId = (long)Command.ExecuteScalar();

            dBConnect.Close();
            return lastRowId;
        }

        /// <summary>
        /// Writes a command to the dbSource
        /// </summary>
        /// <param name="command">string command</param>
        public void WriteToDB(string command)
        {
            SQLiteConnection dBConnect = new SQLiteConnection(string.Format(ITUtil.Common.Constants.Culture.DefaultCulture, "Data source = {0}{1}; Version = 3; ", this.dbPath, this.dbName));
            dBConnect.Open();
            SQLiteCommand Command = new SQLiteCommand(command, dBConnect);
            Command.ExecuteNonQuery();
            dBConnect.Close();
        }

        /// <summary>
        /// The commands for creating the DB Tables
        /// </summary>
        private void CreateTables()
        {
            string logs = @"CREATE TABLE Logs (LogsID integer primary key AUTOINCREMENT, Route varchar, Data varchar, Date varchar);";
            string headers = @"CREATE TABLE Headers (HeadersID integer primary key AUTOINCREMENT, LogsID integer, TokenId varchar, UserId varchar, ValidUntil varchar, IsValid integer, foreign key(LogsID) REFERENCES Logs(LogsID));";
            string claims = @"CREATE TABLE Claims (ClaimsID integer primary key AUTOINCREMENT, HeadersID integer, ClaimKey varchar, ClaimValue varchar, ClaimScope varchar, foreign key(HeadersID) REFERENCES Headers(HeadersID));";

            this.WriteToDB(logs);
            this.WriteToDB(headers);
            this.WriteToDB(claims);
        }
    }
}
