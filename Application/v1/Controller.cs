﻿namespace Monosoft.Service.EventLogger.V1
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using RabbitMQ.Client;
    using RabbitMQ.Client.Events;

    /// <summary>
    /// Controller.
    /// </summary>
    public class Controller : IMicroservice
    {
        /// <summary>
        /// Settings.
        /// </summary>
        public static Settings Settings = Logic.LoadSettings();

        /// <summary>
        /// DbName.
        /// </summary>
        public static string DbName = string.Empty;

        /// <summary>
        /// db.
        /// </summary>
        public static Database Db;

        private const string Servicename = "EventLogger";
        private static IConnection connection;
        private static IModel channel;
        private static string queueName;
        private static EventingBasicConsumer consumer;

        // private static MetaDataDefinition AdminClaim = new MetaDataDefinition(Servicename, "administrator", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("en", "User is allowed to setup svg settings or see svg settings"));

        /// <inheritdoc/>
        public string ServiceName
        {
            get
            {
                return Servicename;
            }
        }

        /// <inheritdoc/>
        public ProgramVersion ProgramVersion
        {
            get
            {
                return new ProgramVersion("1.0.0.0");
            }
        }

        /// <summary>
        /// Gets OperationDescription
        /// </summary>
        public List<OperationDescription> OperationDescription
        {
            get
            {
                return new List<OperationDescription>()
                {
                   new OperationDescription(
                       "Settings",
                       "To Be Removed: Logs Messages to DB",
                       typeof(Settings), // In
                       null, // Out
                       new MetaDataDefinitions(Array.Empty<MetaDataDefinition>()),
                       ChangeSettings),
                };
            }
        }

        /// <summary>
        /// ChangeSettings
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ChangeSettings(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var input = MessageDataHelper.FromMessageData<Settings>(wrapper.messageData);
            Logic.SaveSettings(input);
            Settings = Logic.LoadSettings();

            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Controller"/> class.
        /// Constructor to Setup The Logging
        /// </summary>
        public Controller()
        {
            this.SetupRabbit();
        }

        /// <summary>
        /// Sets up Rabbitmq to log messages
        /// </summary>
        private void SetupRabbit()
        {
            ConnectionFactory factory = new ConnectionFactory()
            {
                HostName = MicroServiceConfig.getConfig(null).RabbitMq.Server,
                UserName = MicroServiceConfig.getConfig(null).RabbitMq.Username,
                Password = MicroServiceConfig.getConfig(null).RabbitMq.Password,
            }; // Rabbit connection setup
            factory.AutomaticRecoveryEnabled = true;
            factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(20);
            connection = factory.CreateConnection(); // save connection
            channel = connection.CreateModel(); // save the connection channel
            channel.ExchangeDeclare(exchange: "ms.request", type: "topic"); // Declare new Exchange: logs
            queueName = channel.QueueDeclare("ms.queue.EventLogger", true, false, false).QueueName; // Declare new Queue with random name and get name of queue
            channel.QueueBind(queue: queueName, exchange: "ms.request", routingKey: "#"); // Binds the queue
            consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                this.Received(model, ea);
            };
            channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);
        }

        /// <summary>
        /// Handles on message received
        /// </summary>
        /// <param name="model">object model</param>
        /// <param name="ea">BasicDeliverEventArgs ea</param>
        private void Received(object model, BasicDeliverEventArgs ea)
        {
            if (DbName != DateTime.Now.ToString("yyyy-MM-dd", ITUtil.Common.Constants.Culture.DefaultCulture) + Settings.dbBaseName)
            {
                Settings = Logic.LoadSettings();
                DbName = DateTime.Now.ToString("yyyy-MM-dd", ITUtil.Common.Constants.Culture.DefaultCulture) + Settings.dbBaseName;
                Db = new Database(DbName, Settings.dbPath); // Prepare the Database
            }

            byte[] body = ea.Body; // MessageWrapper
            MessageWrapper messageWrapper = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageWrapper>(Encoding.UTF8.GetString(body));
            string routingKey = ea.RoutingKey; // Route

            Logic logic = new Logic();
            logic.WriteReceived(messageWrapper, routingKey);
        }
    }
}
