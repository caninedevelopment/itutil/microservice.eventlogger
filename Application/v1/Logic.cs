﻿namespace Monosoft.Service.EventLogger.V1
{
    using System;
    using ITUtil.Common.DTO;

    /// <summary>
    /// Logic
    /// </summary>
    public class Logic
    {
        private const string Settingsfile = "Settings.json";

        /// <summary>
        /// Sets the current settings
        /// </summary>
        /// <returns>Settings</returns>
        public static Settings LoadSettings()
        {
            if (System.IO.File.Exists(Settingsfile))
            {
                var json = System.IO.File.ReadAllText(Settingsfile);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<Settings>(json);
            }
            else
            {
                return new Settings();
            }
        }

        /// <summary>
        /// Saves new settings
        /// </summary>
        /// <param name="value">Settings value</param>
        public static void SaveSettings(Settings value)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            System.IO.File.WriteAllText(Settingsfile, json);
        }

        /// <summary>
        /// Writes the data to the database
        /// </summary>
        /// <param name="messageWrapper">MessageWrapper messageWrapper</param>
        /// <param name="routeingKey">string routeingKey</param>
        public void WriteReceived(MessageWrapper messageWrapper, string routeingKey)
        {
            if (messageWrapper == null)
            {
                throw new ArgumentNullException(nameof(messageWrapper));
            }

            string date = DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss", ITUtil.Common.Constants.Culture.DefaultCulture); // Date
            byte[] message = messageWrapper.messageData; // Data
            TokenData header = messageWrapper.headerInfo; // Header

            // Writes to the logs table in db
            string writeLog = string.Format(ITUtil.Common.Constants.Culture.DefaultCulture, @"Insert INTO Logs (Route,Data,Date) VALUES ('{0}','{1}','{2}');", routeingKey, Convert.ToBase64String(message), date);
            long logId = Controller.Db.WriteToDBReturnID(writeLog);

            // Writes to the Headers table in db
            string writeHeader = string.Format(ITUtil.Common.Constants.Culture.DefaultCulture, @"Insert INTO Headers(LogsID,TokenId,UserId,ValidUntil,IsValid) VALUES ({0},'{1}','{2}','{3}',{4});", logId, header.tokenId.ToString(), header.userId.ToString(), header.validUntil.ToString(ITUtil.Common.Constants.Culture.DefaultCulture), Convert.ToInt32(header.IsValidToken()));
            long headerId = Controller.Db.WriteToDBReturnID(writeHeader);

            // Writes to the Claims table in db
            foreach (var claim in header.claims)
            {
                string writeClaim = string.Format(ITUtil.Common.Constants.Culture.DefaultCulture, @"Insert INTO Claims(HeaderID,ClaimKey,ClaimValue,ClaimScope) VALUES ({0},'{1}','{2}','{3}');", headerId, claim.key, claim.value, claim.scope);
                Controller.Db.WriteToDB(writeClaim);
            }
        }
    }
}
