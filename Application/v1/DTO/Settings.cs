﻿namespace Monosoft.Service.EventLogger.V1
{
    /// <summary>
    /// Settings.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1307:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class Settings
    {
        /// <summary>
        /// dbPath.
        /// </summary>
        public string dbPath = string.Empty;

        /// <summary>
        /// dbBaseName.
        /// </summary>
        public string dbBaseName = "RabbitMQLogDB";
    }
}
